" Vim color file
" Maintainer:	jjtu.china<guochaoxxl@163.com>
" Last Change:	$Date: 2016/03/26 19:30:30 $
" Last Change:	$Date: 2017/01/01 19:30:30 $
" URL:		http:.vim
" Version:	$Id: $

set background=dark
let g:colors_name="jjudesert"

hi Normal	guibg=NONE guifg=NONE ctermfg=NONE ctermbg=235 cterm=NONE

" highlight groups
hi Cursor	guibg=khaki guifg=slategrey
hi VertSplit	guibg=#c2bfa5 guifg=grey50 gui=none
hi Folded	guibg=grey30 guifg=gold
hi FoldColumn	guibg=grey30 guifg=tan
hi IncSearch	guifg=slategrey guibg=khaki
"hi LineNr
hi ModeMsg	guifg=goldenrod
hi MoreMsg	guifg=SeaGreen
hi NonText	guifg=LightBlue guibg=grey30
hi Question	guifg=springgreen
hi Search	guibg=peru guifg=wheat
hi SpecialKey	guifg=yellowgreen
hi StatusLine	guibg=#c2bfa5 guifg=black gui=none
hi StatusLineNC	guibg=#c2bfa5 guifg=grey50 gui=none
hi Title	guifg=indianred
hi Visual	gui=none guifg=khaki guibg=olivedrab
"hi VisualNOS
hi WarningMsg	guifg=salmon
"hi WildMenu
"hi Menu
"hi Scrollbar
"hi Tooltip

" syntax highlighting groups
hi Comment	guifg=SkyBlue
hi Constant	guifg=#ffa0a0
hi Identifier	guifg=palegreen
hi Statement	guifg=khaki
hi PreProc	guifg=indianred
hi Type		guifg=darkkhaki
hi Special	guifg=navajowhite

"hi Underlined
hi Ignore	guifg=grey40
"hi Error
hi Todo		guifg=orangered guibg=yellow2

" color terminal definitions
hi SpecialKey	ctermfg=darkgreen
hi NonText	cterm=bold ctermfg=darkblue
hi Directory	ctermfg=darkcyan
hi ErrorMsg	cterm=bold ctermfg=7 ctermbg=1
hi IncSearch	cterm=NONE ctermfg=yellow ctermbg=green
hi Search	cterm=NONE ctermfg=grey ctermbg=blue
hi MoreMsg	ctermfg=darkgreen
hi ModeMsg	cterm=NONE ctermfg=brown
"hi LineNr	 ctermfg=3
hi LineNr	 ctermfg=blue    cterm=bold
hi Question	ctermfg=green
hi StatusLine	cterm=bold,reverse
hi StatusLineNC cterm=reverse
hi VertSplit	cterm=reverse
hi Title	ctermfg=5
hi Visual	cterm=reverse
""hi VisualNOS	cterm=bold,underline
hi VisualNOS	cterm=bold
hi WarningMsg	ctermfg=1
hi WildMenu	ctermfg=0 ctermbg=3
hi Folded	ctermfg=darkgrey ctermbg=NONE
hi FoldColumn	ctermfg=darkgrey ctermbg=NONE
hi DiffAdd	ctermbg=4
hi DiffChange	ctermbg=5
hi DiffDelete	cterm=bold ctermfg=4 ctermbg=6
hi DiffText	cterm=bold ctermbg=1
"hi Comment	cterm=bold ctermfg=darkcyan
hi Comment	 ctermfg=darkgrey       cterm=bold
"hi Constant	ctermfg=brown        
hi Constant	ctermfg=brown    cterm=bold
hi Special	ctermfg=5     
hi Identifier	ctermfg=6
"hi Statement	ctermfg=3
hi Statement	ctermfg=darkred                 cterm=bold  
"hi PreProc	ctermfg=5
hi PreProc	ctermfg=blue                   cterm=bold
"hi Type		ctermfg=2
hi Type		ctermfg=green            cterm=bold
"hi Underlined	cterm=underline ctermfg=5
hi Underlined	ctermfg=235
hi Ignore	cterm=bold ctermfg=7
hi Ignore	ctermfg=darkgrey
hi Error	cterm=bold ctermfg=7 ctermbg=1


"vim: sw=4
